#import <3ds.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int gameMatrix[4][4];
int redoMatrix[4][4];

int screenPosSquares[4][4][2] = { 
									{ {14, 2}, {20, 2}, {26, 2}, {32, 2} },
									{ {14, 9}, {20, 9}, {26, 9}, {32, 9} },
									{ {14, 16}, {20, 16}, {26, 16}, {32, 16} },
									{ {14, 23}, {20, 23}, {26, 23}, {32, 23} }
								};

int colors[2049][2] = {
						[0]    = {37,40},
						[2]    = {31,40},
						[4]    = {33,40},
						[8]    = {32,40},
						[16]   = {36,40},
						[32]   = {34,40},
						[64]   = {35,40},
						[128]  = {37,40},
						[256]  = {37,41},
						[512]  = {37,45},
						[1024] = {37,44},
						[2048] = {37,42}
					};								

//
// Misc. Functions
//

int fillMatrix(int mat[4][4], int fill)
{
	int i, j;
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{
			mat[i][j] = fill;
		}
	}
	return 0;
}

int matEqueals(int mat1[4][4], int mat2[4][4])
{
	int i, j;
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{
			mat1[i][j] = mat2[i][j];
		}
	}
	return 0;
}

int lenHelper(unsigned x) {
    if (x >= 1000000000) return 10;
    if (x >= 100000000)  return 9;
    if (x >= 10000000)   return 8;
    if (x >= 1000000)    return 7;
    if (x >= 100000)     return 6;
    if (x >= 10000)      return 5;
    if (x >= 1000)       return 4;
    if (x >= 100)        return 3;
    if (x >= 10)         return 2;
    return 1;
}

int printMatrix(int mat[4][4])
{
	int i, j;
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{
			int xPos = screenPosSquares[i][j][0];
			int yPos = screenPosSquares[i][j][1];
			int sqVal = mat[i][j];
			int bgC = colors[sqVal][0];
			int fgC = colors[sqVal][1];
			int valLen = lenHelper(sqVal);
			
			printf("\x1b[%d;%dH\x1b[%d;%dm    \x1b[0m", yPos, xPos, bgC, fgC);
			printf("\x1b[%d;%dH\x1b[%d;%dm    \x1b[0m", yPos+1, xPos, bgC, fgC);
			
			if (sqVal == 0) 
			{
				printf("\x1b[%d;%dH\x1b[%d;%dm \x1b[0m", yPos+2, xPos, bgC, fgC);
			} else {
				printf("\x1b[%d;%dH\x1b[%d;%dm%d\x1b[0m", yPos+2, xPos, bgC, fgC, sqVal);
			}
			
			if (valLen < 4) 
			{
				for (int k = 0; k < (4 - valLen); k++)
				{
					printf("\x1b[%d;%dH\x1b[%d;%dm \x1b[0m", yPos+2, xPos+k+valLen, bgC, fgC);
				}
			}
			
			printf("\x1b[%d;%dH\x1b[%d;%dm    \x1b[0m", yPos+3, xPos, bgC, fgC);
			printf("\x1b[%d;%dH\x1b[%d;%dm    \x1b[0m", yPos+4, xPos, bgC, fgC);
		}
	}
	return 0;
}

int fillScreenIn(int bgC)
{
	int i, j;
	for (i = 0; i < 30; i++) 
	{
		for (j = 0; j < 50; j++)
		{
			printf("\x1b[%d;%dH\x1b[%d;30m \x1b[0m", i, j, bgC);
		}
	}
	return 0;
}

int matCmp(int mat1[4][4], int mat2[4][4])
{
	int i, j;
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{
			if (mat1[i][j] != mat2[i][j])
			{
				return 1;
			}
		}
	}
	return 0;
}

//
// Game Logic
//

//
// Redo Move

int redoMove()
{
	matEqueals(gameMatrix, redoMatrix);
	printMatrix(gameMatrix);
	return 0;
}

//
// Presses

int pressLeft(int mat[4][4])
{
	matEqueals(redoMatrix, gameMatrix);
	moveLeft(mat);
	evalLeft(mat);
	moveLeft(mat);
	endCycle();
	return 0;
}

int pressUp(int mat[4][4])
{
	matEqueals(redoMatrix, gameMatrix);
	moveUp(mat);
	evalUp(mat);
	moveUp(mat);
	endCycle();
	return 0;
}

int pressRight(int mat[4][4])
{
	matEqueals(redoMatrix, gameMatrix);
	moveRight(mat);
	evalRight(mat);
	moveRight(mat);
	endCycle();
	return 0;
}

int pressDown(int mat[4][4])
{
	matEqueals(redoMatrix, gameMatrix);
	moveDown(mat);
	evalDown(mat);
	moveDown(mat);
	endCycle();
	return 0;
}

//
// Moves

int moveLeft(int mat[4][4])
{
	int i, j;
	for (i = 0; i < 4; i++) 
	{
		int ordVals[4];
		int nZ = 0;
		for (j = 0; j < 4; j++)
		{	
			if (mat[i][j] == 0)
			{
				nZ = nZ + 1;
			} else {
				ordVals[j - nZ] = mat[i][j];
			}
		}
		for (int k = 0; k < 4 - nZ; k++) 
		{
			mat[i][k] = ordVals[k];
		}
		for (int k = 0; k < nZ; k++) 
		{
			mat[i][3 - k] = 0;
		}
	}
	
	return 0;
}

int moveUp(int mat[4][4])
{
	int i, j;
	int transpose[4][4];
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{
			transpose[i][j] = mat[j][i];
		}
	}
	
	for (i = 0; i < 4; i++) 
	{
		int ordVals[4];
		int nZ = 0;
		for (j = 0; j < 4; j++)
		{	
			if (transpose[i][j] == 0)
			{
				nZ = nZ + 1;
			} else {
				ordVals[j - nZ] = transpose[i][j];
			}
		}
		for (int k = 0; k < 4 - nZ; k++) 
		{
			transpose[i][k] = ordVals[k];
		}
		for (int k = 0; k < nZ; k++) 
		{
			transpose[i][3 - k] = 0;
		}
	}
	
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{
			mat[i][j] = transpose[j][i];
		}
	}
	return 0;
}

int moveRight(int mat[4][4])
{
	int i, j;
	for (i = 0; i < 4; i++) 
	{
		int ordVals[4];
		int nZ = 0;
		for (j = 0; j < 4; j++)
		{	
			if (mat[i][j] == 0)
			{
				nZ = nZ + 1;
			} else {
				ordVals[j - nZ] = mat[i][j];
			}
		}
		for (int k = 0; k < nZ; k++) 
		{
			mat[i][k] = 0;
		}
		for (int k = 0; k < 4 - nZ; k++) 
		{
			mat[i][k + nZ] = ordVals[k];
		}
	}
	
	return 0;
}

int moveDown(int mat[4][4])
{
	int i, j;
	int transpose[4][4];
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{
			transpose[i][j] = mat[j][i];
		}
	}
	
	for (i = 0; i < 4; i++) 
	{
		int ordVals[4];
		int nZ = 0;
		for (j = 0; j < 4; j++)
		{	
			if (transpose[i][j] == 0)
			{
				nZ = nZ + 1;
			} else {
				ordVals[j - nZ] = transpose[i][j];
			}
		}
		for (int k = 0; k < nZ; k++) 
		{
			transpose[i][k] = 0;
		}
		for (int k = 0; k < 4 - nZ; k++) 
		{
			transpose[i][k + nZ] = ordVals[k];
		}
	}
	
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{
			mat[i][j] = transpose[j][i];
		}
	}
	return 0;
}

//
// Evals

int evalLeft(int mat[4][4])
{
	int i, j;
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{	
			if (j > 0)
			{
				if (mat[i][j] == mat[i][j-1])
				{
					mat[i][j-1] = mat[i][j]*2;
					mat[i][j] = 0;
				}
			}
		}
	}
	return 0;
}

int evalUp(int mat[4][4])
{
	int i, j;
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{	
			if (i > 0)
			{
				if (mat[i][j] == mat[i - 1][j])
				{
					mat[i - 1][j] = mat[i][j]*2;
					mat[i][j] = 0;
				}
			}
		}
	}
	return 0;
}

int evalRight(int mat[4][4])
{
	int i, j;
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{	
			if (j < 3)
			{
				if (mat[i][j] == mat[i][j+1])
				{
					mat[i][j] = mat[i][j]*2;
					mat[i][j + 1] = 0;
				}
			}
		}
	}
	return 0;
}

int evalDown(int mat[4][4])
{
	int i, j;
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < 4; j++)
		{	
			if (i < 3)
			{
				if (mat[i][j] == mat[i + 1][j])
				{
					mat[i + 1][j] = mat[i][j]*2;
					mat[i][j] = 0;
				}
			}
		}
	}
	return 0;
}

//
// End Cycle

int endCycle() 
{
	int openSpaces = 0;
	for (int i = 0; i < 4; i++) 
	{
		for (int j = 0; j < 4; j++)
		{	
			if (gameMatrix[i][j] == 0)
			{
				openSpaces = openSpaces + 1;
			}
		}
	}
	if (openSpaces == 0) 
	{
		//Game Over
	}
	
	if (matCmp(redoMatrix, gameMatrix) != 0)
	{
		int spawnPos = rand() % openSpaces;
		
		int createdValue = rand() % 5;
		if (createdValue == 0) 
		{
			createdValue = 4;
		} else {
			createdValue = 2;
		}
		
		int x = 0;
		bool found = false;
		for (int i = 0; i < 4; i++) 
		{
			if (found) 
			{
				break;
			}
			for (int j = 0; j < 4; j++)
			{	
				if (found) 
				{
					break;
				}
				if (gameMatrix[i][j] == 0)
				{
					if (x == spawnPos)
					{
						gameMatrix[i][j] = createdValue;
						found = true;
					} else {
						x = x + 1;
					}
				}
			}
		}			
	}
	
	printMatrix(gameMatrix);
	return 0;
}

// 
// Game Init
//

int initGame() 
{
	srand(time(NULL));
	fillMatrix(gameMatrix, 0);
	
	int initialX, initialY;
	initialX = rand() % 4;
	initialY = rand() % 4;
	
	gameMatrix[initialY][initialX] = 2;
	return 0;
}