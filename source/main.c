#import <3ds.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#import "g2048.h"

#define BG_COLOR 47

int main() 
{
	gfxInitDefault();
	consoleInit(GFX_TOP, NULL);
	
	fillScreenIn(BG_COLOR);
	
	initGame();
	printMatrix(gameMatrix);
	
	while (aptMainLoop())
	{
		hidScanInput();
		u32 kDown = hidKeysDown();

		if (kDown & KEY_START) break; // Exit Program
		
		if (kDown & KEY_SELECT) // Restart Game
		{
			fillScreenIn(BG_COLOR);
			initGame();
			printMatrix(gameMatrix);			
		}
		
		// Game Inputs
		if (kDown & KEY_LEFT) pressLeft(gameMatrix);
		if (kDown & KEY_UP) pressUp(gameMatrix);
		if (kDown & KEY_RIGHT) pressRight(gameMatrix);
		if (kDown & KEY_DOWN) pressDown(gameMatrix);
		if (kDown & KEY_A) redoMove();
		
		gfxFlushBuffers();
		gfxSwapBuffers();
		gspWaitForVBlank();
	}
	
	gfxExit();
	return 0;
}